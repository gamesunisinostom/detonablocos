#include "libUnicornio.h"

enum Status{
	telaIni, startGame, onGame, endGame, inRecords, outGame, telaCreditos, pauseGame, instruc
};

class Menu
{
public:
    Menu();
    ~Menu();

    void inicializar();
    void finalizar();

    void atualiza(Status &stats);
    void desenhar();

private:
    BotaoSprite jogar;
	BotaoSprite records;
	BotaoSprite credits;
    BotaoSprite sair;
	Musica musica1;
    Sprite logo, backgrd;
    Som somHover, saindo, playSound;
    bool tocou, onJogar = false, onRecords = false, onCredits = false, onSair = false;

    void hoverMenu();
    void soundHover();
};