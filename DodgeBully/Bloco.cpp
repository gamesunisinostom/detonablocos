#include "Bloco.h"

Bloco::Bloco() {}

Bloco::~Bloco() {}

void Bloco::inicializar(int x, int y, int bloco, int alt, int larg)
{
    PosXMatriz = x;
    PosYMatriz = y;
    altura = alt;
    largura = larg;
    hitPoints = bloco;
    changeColor();
}

void Bloco::desenhar(float bolaX, float bolaY, int altura, int largura, char *bolaDirectX, char *bolaDirectY, bool *colideBloco, BounceWith *redirect, int *pontos)
{
    if (hitPoints > 0) {
        testeColisao(bolaX, bolaY, bolaDirectX, bolaDirectY, colideBloco, redirect, pontos);
        uniDesenharRetangulo(PosXMatriz, PosYMatriz, 0.0, largura, altura, 0.5, 0.5, rgb[0], rgb[1], rgb[2]);
    }
}

void Bloco::testeColisao(float bolaX, float bolaY, char *bolaDirectX, char *bolaDirectY, bool *colideBloco, BounceWith *redirect, int *pontos)
{
    bool colide = validaColisao.colisoesBloco(bolaX, bolaY, PosXMatriz, PosYMatriz, altura, largura);

    if (colide) {
        *colideBloco = true;
        *redirect = validaColisao.redirectBloco(bolaDirectX, bolaDirectY, bolaX, bolaY, PosXMatriz, PosYMatriz, redirect);
        hitPoints -= 1;
        *pontos += 10;
		changeColor();
    }
}

void Bloco::changeColor()
{
    switch (hitPoints) {
    case 0:
        tipo = voided;
        break;
    case 1:
        tipo = green;
        rgb[0] = neonGreen[0];
        rgb[1] = neonGreen[1];
        rgb[2] = neonGreen[2];
        break;
    case 2:
        tipo = yellow;
        rgb[0] = neonYellow[0];
        rgb[1] = neonYellow[1];
        rgb[2] = neonYellow[2];
        break;
    case 3:
        tipo = blue;
        rgb[0] = neonBlue[0];
        rgb[1] = neonBlue[1];
        rgb[2] = neonBlue[2];
        break;
    case 4:
        tipo = red;
        rgb[0] = neonOrange[0];
        rgb[1] = neonOrange[1];
        rgb[2] = neonOrange[2];
        break;
    case 5:
        tipo = red;
        rgb[0] = neonPink[0];
        rgb[1] = neonPink[1];
        rgb[2] = neonPink[2];
        break;
    }
}