#include <windows.h>
#include "Jogo.h"

Jogo::Jogo() {}

Jogo::~Jogo() {}

void Jogo::inicializar()
{
    uniInicializar(GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN), true, "NeonWars");
    status = telaIni;
    pontos = 0;
    nivel = 1;
    vidas = 3;

    recursos.carregarAudio("colisao", "./sons/colisao1.wav");
    recursos.carregarAudio("colisao2", "./sons/colisao2.wav"); 
    recursos.carregarAudio("colisaoBase", "./sons/colidBase.wav"); 
    recursos.carregarSpriteSheet("back", "./imagens/space-with-stars2.jpg", 1, 1);

    recursos.carregarSpriteSheet("bkg1", "./imagens/bkg1.jpg", 1, 1);
    recursos.carregarSpriteSheet("bkg2", "./imagens/bkg2.png", 1, 1);
    recursos.carregarSpriteSheet("bkg3", "./imagens/bkg3.jpg", 1, 1);
    recursos.carregarSpriteSheet("bkg4", "./imagens/bkg4.jpg", 1, 1);
	recursos.carregarSpriteSheet("bkg5", "./imagens/bkg5.jpg", 1, 1);
    recursos.carregarSpriteSheet("inst", "./imagens/ajuda.png", 1, 1);

    recursos.carregarSpriteSheet("gameOver", "./imagens/gameOver2.png", 1);
    recursos.carregarSpriteSheet("base", "./imagens/sabre.png", 1, 1);
    recursos.carregarFonte("fantasque", "./fonts/stjedise/STJEDISE.TTF", 28);

    fonte.setFonte("fantasque");
    tela.inicializar(fonte);

	instrucao.setSpriteSheet("inst");
    gameOverSprite.setSpriteSheet("gameOver");
    backgrd.setSpriteSheet("back");
    baseSabre.setSpriteSheet("base");
    baseSabre.setEscala(0.5,1);

    menuIni.carregar("./sons/CantinaSong.ogg");
    gameTheme.carregar("./sons/StarWars.ogg");
    menuIni.setVolume(80);
    gameTheme.setVolume(80);

    menuIni.tocar();
    while (!recursos.carregouAudio("colisao")) {
        tela.carregando();
    }

    speed = 2.0;

    menu.inicializar();
    bola.iniciaBola();
    fase.inicializar(1);
}

void Jogo::startAgain()
{
    menuIni.parar();
    gameTheme.tocar();

    bola.iniciaBola();
    fase.inicializar(1);
    pontos = 0;
    nivel = 1;
    vidas = 3;
    speed = 2.0;
	status = instruc;
}

void Jogo::finalizar()
{
    fase.finalizar();
    menu.finalizar();
    tela.finalizar();
    menuIni.descarregar();
    gameTheme.descarregar();
    recursos.descarregarTudo();
	uniFinalizar();
}

void Jogo::executar()
{
    xBase = 400;
    

	while(!teclado.soltou[TECLA_ESC] && !aplicacao.sair && status != outGame)
	{
		uniIniciarFrame();

        if (status != onGame) {
            backgrd.desenhar(janela.getLargura() / 2, janela.getAltura() / 2);
            mouse.mostrarCursor();
        }

        switch (status) {
            case telaIni:
                menu.desenhar();
                menu.atualiza(status);
                break;
            case inRecords:
                tela.loadRecords();
                tela.recordScreen();
                backIni();
                break;
            case startGame:
                startAgain();
			case instruc:
                instrucao.desenhar(janela.getLargura() / 2, janela.getAltura() / 2);
				if (teclado.pressionou[TECLA_ENTER]) {
					status = onGame;
				}
				break;
            case onGame:
                playGame();
                break;
            case telaCreditos:
                tela.showCreditos();
                backIni();
                break;
            case endGame:
                gameOver();
                backIni();
        }
		
		uniTerminarFrame();
	}
}

void Jogo::backIni()
{
    if (teclado.pressionou[TECLA_ENTER]) {
        if (status == endGame) {
            gameTheme.parar();
            menuIni.tocar();
        }
        status = telaIni;
    }
}

void Jogo::playGame() 
{
    tela.setInterface(nivel, pontos, vidas);
    oldNivel = nivel;
    int oldPontos = pontos;

    if (vidas < 1) {
        tela.loadRecords();
        status = endGame;
        return;
    }

    /* Aviso de gambiarra */
    if (nivel > 1) {
        fase.setAncoras(60, 60);
    }

    mouse.esconderCursor();
    mx = mouse.x;

    if (teclado.soltou[TECLA_R]) {
        bola.iniciaBola();
		fase.finalizar();
		fase.inicializar(nivel);
    } 
    if (teclado.soltou[TECLA_P]) {
        bola.iniciaBola();
        fase.finalizar();
        nivel++;
        if (nivel > 10){
            nivel = 1;
        }
        fase.inicializar(nivel);
    }

    if (mouse.pressionou[0] && bola.getOnBase()) {
        bola.setDirections('e', 's');
        bola.setOnBase(false);
    }
    else if (mouse.pressionou[2] && bola.getOnBase()) {
        bola.setDirections('d', 's');
        bola.setOnBase(false);
    }

    if (mx >= 50 && mx <= janela.getLargura() - 50) {
        xBase = mx;
    }

    fase.desenhar(bola.getX(), bola.getY(), &bola.DirectX, &bola.DirectY, &bola.colidiuBloco, &bola.colisionDirect, &nivel, &pontos);
    if (oldNivel < nivel) {
        speed += 0.3;
        bola.iniciaBola();
    }
	bola.desenhar(&speed, mx, janela.getLargura(), xBaseOld, xBase, &vidas);

    uniDesenharRetangulo(xBase, janela.getAltura() * 0.85, 0.0, 100, 10, 0.5, 0.5, 150, 100, 100);
    baseSabre.desenhar(xBase, janela.getAltura() * 0.85);
    tela.desenha();

    if (pontos % 1000 == 0 && pontos > 0 && oldPontos % 1000 == 1) {
        speed += 0.1;
    }
	xBaseOld = mx;
}

void Jogo::gameOver()
{
    tela.saveRecords(pontos);
}