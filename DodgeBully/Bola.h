#include "Validator.h"

enum Posicao {
    posX, posY, posYInicial
};

class Bola
{
public:
    Bola();
    ~Bola();
    void iniciaBola();
    void setOnBase(bool state);
    void setDirections(char dirX, char dirY);
    void setDirectionX(char dirX);
    void setDirectionY(char dirY);
	void desenhar(float *speed, int mouseX, int larguraJanela, float xBaseOld, float xBase, int *vidas);
    float getX();
    float getY();
    bool getOnBase();

    char DirectX, DirectY;
    bool colidiuBloco;
    BounceWith colisionDirect;

protected:
    float posicoes[3];
    bool fixaBase, bounce, reflect;
    Validator validador;
    Som baseColid;
};

