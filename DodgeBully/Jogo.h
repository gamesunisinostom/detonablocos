#include "libUnicornio.h"
#include "Bola.h"
#include "Base.h"
#include "Fase.h"
#include "Menu.h"
#include "Tela.h"

class Jogo
{
public:
	Jogo();
	~Jogo();

	void inicializar();
	void finalizar();
    void playGame();

	void executar();
protected:
    int mx, xBase, xBaseOld, pontos, nivel, vidas , oldNivel;
    float speed;
    Sprite backgrd, gameOverSprite, baseSabre, instrucao;
    Bola bola;
	Base base;
    Fase fase;
    Tela tela;
    Texto fonte;
    Status status;
    Menu menu;
    Musica menuIni, gameTheme;

    void gameOver();
    void backIni();
    void startAgain();
};

