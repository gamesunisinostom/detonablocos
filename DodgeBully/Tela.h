#include "libUnicornio.h"
#include <iomanip>
#include <locale>
#include <sstream>
#include <iostream>
#include <fstream>

class Tela 
{
public:
    Tela();
    ~Tela();
    void finalizar();
    void setInterface(int nNivel, int nPonto, int nVida);
    void carregando();
    void desenha();
    void loadRecords();
    void saveRecords(int pontos);
    void recordScreen();
    void inicializar(Texto fonte);
    void setNivel(int nivel);
    void setPontos(int pontos);
    void setVidas(int vidas);
    void showCreditos();

protected:
    Texto pontos, vidas, nivel, creditos, records;
    int tempoAnima, *topRecords, colocacao;
    bool animacao, novoRecord;
    ifstream arqRecords;
    ofstream arqSaveRecords;

    void montarecorScreen(int pos, int pontos);
};