#include "Bola.h"

Bola::Bola() {}

Bola::~Bola() {}

void Bola::iniciaBola() {
    posicoes[posX] = 400;
    posicoes[posYInicial] = posicoes[posY] = janela.getAltura() * 0.82;
    fixaBase = true;
    bounce = false;
    colidiuBloco = false;
    baseColid.setAudio("colisaoBase");
    baseColid.setVolume(50);
	
}

void Bola::setOnBase(bool state) {
    fixaBase = state;
}
bool Bola::getOnBase() {
    return fixaBase;
}

void Bola::desenhar(float *speed, int mouseX, int larguraJanela, float xBaseOld, float xBase, int *vidas) {
    if (fixaBase){
        if (mouseX >= 50 && mouseX <= larguraJanela - 50) {
            posicoes[posX] = mouseX;
        }
    } else {

		/* Aviso de gambiarra */
		float speedY = *speed;

		colisionDirect = validador.colisoesBola(&bounce, &DirectX, &DirectY, posicoes[posX], posicoes[posY], xBaseOld, xBase, &colidiuBloco, &colisionDirect, vidas, &reflect);

		if (reflect) {
			speedY += 2;
		}

        // Verifica se a bola ainda n�o bateu contra nenhum canto, assim ela ter� seu primeiro movimento definido
        if (!bounce) {
            if (DirectX == 'e') {
				posicoes[posY] -= speedY + 3;
                posicoes[posX] -= *speed;
            } else {
				posicoes[posY] -= speedY;
                posicoes[posX] += *speed;
            }
        } 
        switch (colisionDirect) {
            case Top:
                if (DirectX == 'e') {
					posicoes[posY] += speedY;
                    posicoes[posX] -= *speed;
                } else {
					posicoes[posY] += speedY;
                    posicoes[posX] += *speed;
                }
                break;
            case Esq:
                if (DirectY == 's') {
					posicoes[posY] -= speedY;
                    posicoes[posX] += *speed;
                } else  {
					posicoes[posY] += speedY;
                    posicoes[posX] += *speed;
                }
                break;
            case Dir:
                if (DirectY == 's') {
					posicoes[posY] -= speedY;
                    posicoes[posX] -= *speed;
                } else  {
					posicoes[posY] += speedY;
                    posicoes[posX] -= *speed;
                }
                break;
            case colidBase:
                if (DirectX == 'e') {
					posicoes[posY] -= speedY;
                    posicoes[posX] -= *speed;
                } else {
					posicoes[posY] -= speedY;
                    posicoes[posX] += *speed;
                }
                break;
            case resetBall:
                *speed += 0.2;
                iniciaBola();
                break;
            }
        }

    uniDesenharCirculo(posicoes[posX], posicoes[posY], 10, 25, 0, 255, 255);

    if (colisionDirect == colidBase) {
        if (!baseColid.estaTocando()) {
            baseColid.tocar(false);
        }
    }
}

void Bola::setDirections(char dirX, char dirY) {
    DirectX = dirX;
    DirectY = dirY;
}
void Bola::setDirectionX(char dirX) {
    DirectX = dirX;
}
void Bola::setDirectionY(char dirY) {
    DirectY = dirY;
}

float Bola::getX() {
    return posicoes[posX];
}

float Bola::getY() {
    return posicoes[posY];
}