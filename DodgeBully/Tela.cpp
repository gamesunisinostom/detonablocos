#include "Tela.h"

Tela::Tela() { }

Tela::~Tela() { }

void Tela::inicializar(Texto fonte)
{
    tempoAnima = 0;
    animacao = false;

    nivel.setFonte(fonte.getFonte());
    pontos.setFonte(fonte.getFonte());
    vidas.setFonte(fonte.getFonte());
    creditos.setFonte(fonte.getFonte());
    records.setFonte(fonte.getFonte());

    nivel.setCor(255, 224, 89);
    pontos.setCor(255, 224, 89);
    vidas.setCor(255, 224, 89);
    creditos.setCor(255, 255, 255);
    records.setCor(255, 255, 255);

    nivel.setAncora(0, 0);
    pontos.setAncora(0, 0);
    vidas.setAncora(0, 0);
    creditos.setAncora(0.5, 0.5);
    records.setAncora(0.5, 0.5);

    nivel.setAlinhamento(TEXTO_ALINHADO_A_ESQUERDA);
    pontos.setAlinhamento(TEXTO_ALINHADO_A_ESQUERDA);
    vidas.setAlinhamento(TEXTO_ALINHADO_A_ESQUERDA);
    creditos.setAlinhamento(TEXTO_CENTRALIZADO);
    records.setAlinhamento(TEXTO_CENTRALIZADO);

    nivel.setEspacamentoLinhas(1);
    pontos.setEspacamentoLinhas(1);
    vidas.setEspacamentoLinhas(1);
    creditos.setEspacamentoLinhas(1);
    records.setEspacamentoLinhas(1);
    
    nivel.setString("Nivel: ");
    pontos.setString("Pontos: ");
    vidas.setString("Vidas: ");

    loadRecords();
}

void Tela::setInterface(int nNivel, int nPonto, int nVida)
{
    setNivel(nNivel);
    setPontos(nPonto);
    setVidas(nVida);
}

void Tela::carregando()
{
    //Aten��o - Gambiarra a frente
    creditos.adicionarString("  --  -- Loading --  --  ");
    creditos.desenhar(janela.getLargura() / 2, janela.getAltura() / 2);
}

void Tela::showCreditos()
{
    creditos.setString(" -- Desenvolvedores --\nThomas Souza && Ramass�s Rodrigues\n :D");
    creditos.adicionarString("\nDiretor de Arte: Ramass�s Rodrigues\nDiretor de Programa��o: Thomas Souza\nLevel Design: Ramass�s Rodrigues");
    creditos.adicionarString("\n Sonoplastia: Matheus Souza\nTester: Ramass�s Rodrigues");
    creditos.adicionarString("\n\n -- Agradecimentos Especiais --\nD�bora Matheus\n Matheus Souza");
    creditos.adicionarString("\n\nAperte ENTER para voltar");
    creditos.desenhar(janela.getLargura() / 2, janela.getAltura() / 2);
}

void Tela::setNivel(int niv)
{
    string mudada = nivel.getString();
    ostringstream convert;
    convert << niv; 
    mudada = "Nivel: " + convert.str();
    nivel.setString(mudada);
}
void Tela::setPontos(int pont)
{
    string mudada = pontos.getString();
    ostringstream convert;
    convert << pont;
    mudada = "Pontos: " + convert.str();
    pontos.setString(mudada);
}
void Tela::setVidas(int vid)
{
    string mudada = vidas.getString();
    ostringstream convert;
    convert << vid;
    mudada = "vidas: " + convert.str();
    vidas.setString(mudada);
}

void Tela::desenha()
{
    nivel.desenhar(janela.getLargura() * 0.01, janela.getAltura() * 0.9);
    vidas.desenhar(janela.getLargura() * 0.5, janela.getAltura() * 0.9);
    pontos.desenhar(janela.getLargura() * 0.8, janela.getAltura() * 0.9);
}

void Tela::loadRecords()
{
    arqRecords.open("records.txt");

    if (!arqRecords.is_open()) {
        records.setString("o jogo ainda n�o possui records");
        topRecords = new int[10];
        for (int top = 0; top < 10; top++) {
            topRecords[top] = 0;
        }
    } else{
        topRecords = new int[10];
        records.setString("Records \n\n");
        for (int top = 0; top < 10; top++) {
            arqRecords >> topRecords[top];
            if (topRecords[top] != 0) {
                montarecorScreen(top, topRecords[top]);
            }
        }
    }

    novoRecord = false;

    arqRecords.close();
}

void Tela::saveRecords(int pontos)
{
    ostringstream convert;
    string mudada = records.getString();

    for (int top = 0; top < 10; top++) {

        if (novoRecord) {
            continue;
        }

        if (pontos > topRecords[top]) {
            int final = 9;
            while (final > top) {
                topRecords[final] = topRecords[final - 1];
                final--;
            }
            topRecords[top] = pontos;
            colocacao = top + 1;
            novoRecord = true;
        }
    }

    if (novoRecord) {
        records.setString("novo record\n\n");

        convert << colocacao;
        mudada = convert.str() + " lugar\n ";
        records.adicionarString(mudada);

        ostringstream convert;
            
        convert << pontos;
        mudada = "pontua��o: " + convert.str();
        records.adicionarString(mudada);

        records.desenhar(janela.getLargura() / 2, janela.getAltura() / 2);

        arqSaveRecords.open("records.txt");

        for (int top = 0; top < 10; top++) {
            arqSaveRecords << topRecords[top] << endl;
        }

        arqSaveRecords.close();
    } else {
        records.setString("game over \n\n aperte enter para continuar");
        records.desenhar(janela.getLargura() / 2, janela.getAltura() / 2);
    }

}
void Tela::recordScreen()
{
    records.desenhar(janela.getLargura() / 2, janela.getAltura() / 2);
}

void Tela::montarecorScreen(int pos, int pontos)
{
    ostringstream convertPos, convertPts;
    string mudada = records.getString();

    convertPos << pos + 1;
    records.adicionarString(convertPos.str() + " - ");

    convertPts << pontos;
    records.adicionarString(convertPts.str() + " pts\n");
}

void Tela::finalizar()
{
    delete[] topRecords;
}