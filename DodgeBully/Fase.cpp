#include "Fase.h"

Fase::Fase() {}

Fase::~Fase() {}

void Fase::inicializar(int fase)
{
    iniciada = true;
    finalizada = false;
    faseNumber = fase;
    ancora[ancoraX] = janela.getLargura() * 0.05;
    ancora[ancoraY] = janela.getAltura() * 0.08;
    changeBkgrd();

   loadMatriz();
}

void Fase::loadMatriz()
{
    switch (faseNumber) {
        case 1:
            arqBlocos.open("blocos.txt");
            break;
        case 2:
            arqBlocos.open("blocos2.txt");
            break;
        case 3:
            arqBlocos.open("blocos3.txt");
            break;
        case 4:
            arqBlocos.open("blocos4.txt");
            break;
        case 5:
            arqBlocos.open("blocos5.txt");
            break;
        case 6:
            arqBlocos.open("blocos6.txt");
            break;
        case 7:
            arqBlocos.open("blocos7.txt");
            break;
        case 8:
            arqBlocos.open("blocos8.txt");
            break;
        case 9:
            arqBlocos.open("blocos9.txt");
            break;
        case 10:
            arqBlocos.open("blocos10.txt");
            break;
    }

    if (arqBlocos.is_open()) {

		arqBlocos >> matrizY >> matrizX >> largura >> altura >> distBlocosX >> distBlocosY;

        altura = janela.getAltura() * 0.048;
        largura = janela.getLargura() * 0.048;

        matrizFase = new Bloco *[matrizY];
        for (int linha = 0; linha < matrizY; linha++) {
            matrizFase[linha] = new Bloco [matrizX];
            for (int coluna = 0; coluna < matrizX; coluna++) {
                arqBlocos >> passingBlockType;
				matrizFase[linha][coluna].inicializar(ancora[ancoraX] + coluna * (distBlocosX + largura), ancora[ancoraY] + linha * (distBlocosY + altura) , std::stoi(passingBlockType), altura, largura);
            }
        }
    }

    arqBlocos.close();
}

void Fase::desenhar(float bolaX, float bolaY, char *bolaDirectX, char* bolaDirectY, bool *colideBloco, BounceWith *redirect, int *nivel, int *pontos)
{
    int blocos = 0;

    background.desenhar(janela.getLargura() / 2, janela.getAltura() / 2);

    for (int linha = 0; linha < matrizY; linha++) {
        for (int coluna = 0; coluna < matrizX; coluna++) {
            if (matrizFase[linha][coluna].getTipo()){
                blocos++;
            }
            matrizFase[linha][coluna].desenhar(bolaX, bolaY, altura, largura, bolaDirectX, bolaDirectY, colideBloco, redirect, pontos);
        }
    }

    if (blocos == 0) {
        finalizar();
        faseNumber++;
        *nivel += 1;
        loadMatriz();
        changeBkgrd();
    }

}

void Fase::changeBkgrd()
{
    int randomico = rand() % 5;

    switch (randomico) {
    case 0:
        background.setSpriteSheet("bkg1");
        break;
    case 1:
        background.setSpriteSheet("bkg2");
        break;
    case 2:
        background.setSpriteSheet("bkg3");
        break;
    case 3:
        background.setSpriteSheet("bkg4");
        break;
    case 4:
        background.setSpriteSheet("bkg5");
        break;
    }
}

void Fase::finalizar()
{
    delete[] matrizFase;
}

void Fase::setAncoras(int ancX, int ancY)
{
	ancora[ancoraX] = ancX;
	ancora[ancoraY] = ancY;
}