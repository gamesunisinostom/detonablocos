#include "Validator.h"

Validator::Validator() {}

Validator::~Validator() {}

BounceWith Validator::colisoesBola(bool *bounce, char *dirX, char *dirY, float xBola, float yBola, float xBaseOld, float xBase, bool *colidiuBloco, BounceWith *colisionDirect, int *vidas, bool *reflect)
{
    bool colidBas = uniColisaoCirculoComRetangulo(xBola, yBola + 5, 5, xBase, janela.getAltura() * 0.85, 0.0, 300, 20, 0.5, 0.5, false);

    // Verifica colis�o contra o canto superior da tela, que deve ser uma compara��o de yBola menor ou igual ao dobro do raio da bola
    if (yBola <= 10) {
        *bounce = true;
        bounceTop = true;
        bounceEsq = false;
        colidiuBase = false;
        bounceDir = false;
        *colidiuBloco = false;
		*reflect = false;

        *dirY = 'd';
    }
    // Verifica colis�o contra o canto esquerdo da tela, que deve ser uma compara��o de xBola menor ou igual ao dobro do raio da bola
    if (xBola <= 10) {
        *bounce = true;
        bounceEsq = true;
        bounceTop = false;
        colidiuBase = false;
        bounceDir = false;
        *colidiuBloco = false;
		*reflect = false;

        *dirX = 'd';
    }
    // Verifica colis�o contra o canto direito da tela, que deve ser uma compara��o de xBola maior ou igual ao tamanho da janela menos o dobro do raio da bola
    if (xBola >= janela.getLargura() - 10) {
        *bounce = true;
        bounceDir = true;
        bounceTop = false;
        bounceEsq = false;
        colidiuBase = false;
        *colidiuBloco = false;
		*reflect = false;

        *dirX = 'e';
    }
    // Verifica colis�o contra a base
    if (colidBas) {
        *bounce = true;
        colidiuBase = true;
        bounceTop = false;
        bounceEsq = false;
        bounceDir = false;
        *colidiuBloco = false;
		*reflect = false;

        *dirY = 's';

		if (xBaseOld > xBase) {
			*dirX = 'e';
			*reflect = true;
		} else if (xBaseOld < xBase) {
			*dirX = 'd';
			*reflect = true;
		}
    }
    // Verifica se a bola saiu da tela
    if (yBola > janela.getAltura()) {
        *bounce = false;
        bounceTop = false;
        bounceEsq = false;
        colidiuBase = false;
        bounceDir = false;
        *colidiuBloco = false;
        *vidas -= 1;
        return resetBall;
		*reflect = false;
    }

    if (*colidiuBloco == true) {
		*reflect = false;
        return *colisionDirect;
    }

    if (bounceTop) {
        return Top;
    } else if (bounceDir) {
        return Dir;
    } else if (bounceEsq) {
        return Esq;  
    } else if (colidiuBase) {
        return colidBase;
    }
}

bool Validator::colisoesBloco(int xBola, int yBola, int xBloco, int yBloco, int altura, int largura)
{
    bool colidiu = uniColisaoCirculoComRetangulo(xBola, yBola, 5, xBloco, yBloco, 0.0, largura, altura, 0.5, 0.5, false);

    if (colidiu) {
        if (rand() % 2) {
            somColisao.setAudio("colisao");
        } else {
            somColisao.setAudio("colisao2");
        }

        somColisao.setVolume(75);

        somColisao.tocar();
    }

    return colidiu;
}

BounceWith Validator::redirectBloco(char *dirX, char *dirY, float xBola, float yBola, float xBloco, float yBloco, BounceWith *colisionDirect)
{
    BounceWith retorno;

    if (yBola <= yBloco || yBola + 2 <= yBloco) {
        if (*colisionDirect != Esq && *colisionDirect != Dir) {
            bounceTop = false;
            bounceEsq = false;
            colidiuBase = true;
            bounceDir = false;

            *dirY = 's';
        }
    }

    if (xBola <= xBloco || xBola + 2 <= xBloco) {
        if (*colisionDirect != Top && *colisionDirect != colidBase) {
            bounceEsq = false;
            bounceTop = false;
            colidiuBase = false;
            bounceDir = true;

            *dirX = 'd';
        }
    }

    if (xBola >= xBloco || xBola + 2 >= xBloco) {
        if (*colisionDirect != Top && *colisionDirect != colidBase) {
            bounceDir = false;
            bounceTop = false;
            bounceEsq = true;
            colidiuBase = false;

            *dirX = 'e';
        }
    }

    if (yBola >= yBloco || yBola + 2 >= yBloco) {
        if (*colisionDirect != Esq && *colisionDirect != Dir) {
            bounceTop = true;
            bounceEsq = false;
            colidiuBase = false;
            bounceDir = false;

            *dirY = 'd';
        }
    }

    if (bounceTop == true) {
        retorno = Top;
    }
    if (bounceDir == true) {
        retorno = Dir;
    }
    if (bounceEsq == true) {
        retorno = Esq;
    }
    if (colidiuBase == true) {
        retorno = colidBase;
    }
    return retorno;
}