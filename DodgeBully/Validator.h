#pragma once
#include <ctime>
#include <math.h>
#include "libUnicornio.h"

enum Directions {
    LeftTop, LeftDown, RightTop, RightDown
};

enum BounceWith {
    Top , Esq , Dir, colidBase, resetBall
};

class Validator
{
public:
    Validator();
    ~Validator();
	BounceWith colisoesBola(bool *bounce, char *dirX, char *dirY, float xBola, float yBola, float xBaseOld, float xBase, bool *colidiuBloco, BounceWith *colisionDirect, int *vidas, bool *reflect);
    bool colisoesBloco(int xBola, int yBola, int xBloco, int yBloco, int altura, int largura);
    BounceWith redirectBloco(char *dirX, char *dirY, float xBola, float yBola, float xBloco, float yBloco, BounceWith *colisionDirect);

protected:
    bool click = false, bounceTop = false, bounceEsq = false, bounceDir = false, colidiuBase = false;
    BounceWith colisionDirect;
    Som somColisao;
};