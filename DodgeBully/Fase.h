#include "Bloco.h"
#include "Validator.h"
#include <iostream>
#include <fstream>

enum AncoraFase {
    ancoraX, ancoraY
};

class Fase
{
public:
    Fase();
    ~Fase();
    void inicializar(int numberFase);
    void desenhar(float bolaX, float bolaY, char *bolaDirectX, char *bolaDirectY, bool *colideBloco, BounceWith *redirect, int *nivel, int *pontos);
    void finalizar();
	void setAncoras(int ancX, int ancY);

protected:
    int faseNumber, distBlocosX, distBlocosY, matrizX, matrizY, ancora[2], largura, altura;
    string passingBlockType;
    bool iniciada, finalizada;
    Bloco** matrizFase;
    ifstream arqBlocos;
    ofstream arqAleatorio;
    Sprite background;

    void loadMatriz();
    void changeBkgrd();
};