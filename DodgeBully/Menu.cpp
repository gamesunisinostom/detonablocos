#include "Menu.h"

Menu::Menu()
{
}

Menu::~Menu()
{
}

void Menu::inicializar()
{
    tocou = false;

	//	carrega os spritesheets para os botoes
	recursos.carregarSpriteSheet("botao_start", "./spritesheets/button_start2.png", 3, 1);
	recursos.carregarSpriteSheet("botao_credits", "./spritesheets/button_credits.png", 3, 1);
	recursos.carregarSpriteSheet("botao_records", "./spritesheets/button_records.png", 3, 1);
	recursos.carregarSpriteSheet("botao_exit", "./spritesheets/button_exit.png", 3, 1);
    recursos.carregarSpriteSheet("logo", "./spritesheets/logo.png", 1, 1);
    recursos.carregarAudio("hover1", "./sons/Swing3.wav");
    recursos.carregarAudio("hover2", "./sons/Swing4.wav");
    recursos.carregarAudio("hover3", "./sons/Swing5.wav");
    recursos.carregarAudio("hover4", "./sons/Swing6.wav");
    recursos.carregarAudio("openSaber", "./sons/openSaber.wav");
    recursos.carregarAudio("saindo", "./sons/PowerDown2.wav");

	//	setar spritesheet nos botoes
	jogar.setSpriteSheet("botao_start");
	credits.setSpriteSheet("botao_credits");
    records.setSpriteSheet("botao_records");
	sair.setSpriteSheet("botao_exit");
    logo.setSpriteSheet("logo");
    backgrd.setSpriteSheet("back");

    playSound.setAudio("openSaber");
    saindo.setAudio("saindo");

	//	posiciona os botoes
	jogar.setPos(janela.getLargura() / 2, janela.getAltura() / 2 - 100);
    records.setPos(janela.getLargura() / 2, janela.getAltura() / 2);
	credits.setPos(janela.getLargura() / 2, janela.getAltura() / 2 + 100);
	sair.setPos(janela.getLargura() / 2, janela.getAltura() / 2 + 200);

}

void Menu::finalizar()
{
	//	descarregar spritesheets
	recursos.descarregarSpriteSheet("botao_start");
	recursos.descarregarSpriteSheet("botao_records");
	recursos.descarregarSpriteSheet("botao_credits");
	recursos.descarregarSpriteSheet("botao_exit");
}

void Menu::atualiza(Status &status)
{
	jogar.atualizar();
    records.atualizar();
	credits.atualizar();
	sair.atualizar();

    hoverMenu();   
    
	if (jogar.estaClicado()){
        playSound.tocar();
        uniDormir(1200);
		status = startGame;
	}
    if (records.estaClicado()) {
        status = inRecords;
    }
    if (credits.estaClicado()) {
        status = telaCreditos;
    }
    if (sair.estaClicado()) {
        saindo.tocar();
        uniDormir(1500);
        status = outGame;
    }
}

void Menu::desenhar()
{
    backgrd.desenhar(janela.getLargura() / 2 , janela.getAltura() / 2);
    logo.desenhar(janela.getLargura() / 2, janela.getAltura() + 100);
	//	desenha os botoes
	jogar.desenhar();
    records.desenhar();
	credits.desenhar();
	sair.desenhar();
}

void Menu::hoverMenu()
{
    if (jogar.mouseEntrouEmCima()) {
        soundHover();
        onJogar = true;
        if (!somHover.estaTocando() && !tocou) {
            somHover.tocar(false);
            tocou = true;
        }
    }

    if (jogar.mouseSaiuDeCima()) {
        tocou = false;
    }

    if (records.mouseEntrouEmCima()) {
        soundHover();
        onCredits = true;
        if (!somHover.estaTocando() && !tocou) {
            somHover.tocar(false);
            tocou = true;
        }
    }
    if (records.mouseSaiuDeCima() && onCredits) {
        tocou = false;
    }

    if (credits.mouseEntrouEmCima()) {
        soundHover();
        onCredits = true;
        if (!somHover.estaTocando() && !tocou) {
            somHover.tocar(false);
            tocou = true;
        }
    }

    if (credits.mouseSaiuDeCima() && onCredits) {
        tocou = false;
    }

    if (sair.mouseEntrouEmCima()) {
        soundHover();
        onSair = true;
        if (!somHover.estaTocando() && !tocou) {
            somHover.tocar(false);
            tocou = true;
        }
        
    }
    if (sair.mouseSaiuDeCima() && onSair) {
        tocou = false;
    }
}

void Menu::soundHover()
{
    int randomico = rand() % 4;

    switch (randomico) {
        case 0:
            somHover.setAudio("hover1");
            break;
        case 1:
            somHover.setAudio("hover2");
            break;
        case 2:
            somHover.setAudio("hover3");
            break;
        case 3:
            somHover.setAudio("hover4");
            break;
    }
}