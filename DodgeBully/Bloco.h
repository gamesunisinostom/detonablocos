#include "libUnicornio.h"
#include "Validator.h"

enum BlockType {
    voided, green, yellow, blue, red,
};

const int neonBlue[3] = { 0, 254, 255 };
const int neonGreen[3] = { 42, 255, 96 };
const int neonYellow[3] = { 255, 254, 0 };
const int neonOrange[3] = { 255, 174, 29 };
const int neonPink[3] = { 233, 32, 152 };

class Bloco
{
public:
    Bloco();
    ~Bloco();
    void inicializar( int PosX, int PosY, int tipo_bloco, int alt, int larg );
    void desenhar(float bolaX, float bolaY, int altura, int largura, char *bolaDirectX, char *bolaDirectY, bool *colideBloco, BounceWith *redirect, int *pontos);
    BlockType getTipo() { return tipo; };
    
protected:
    int PosXMatriz, PosYMatriz, hitPoints, rgb[3], largura, altura;
    BlockType tipo;
    Validator validaColisao;

    void testeColisao(float bolaX, float bolaY, char *bolaDirectX, char *bolaDirectY, bool *colideBloco, BounceWith *redirect, int *pontos);
	void changeColor();
};